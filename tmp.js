//const gl = require("./library/gl-cli").GLClient

import {
  about, 
  getGitLabClient, 
  getAllDescendantGroups,
  allDescendantGroups,
  allProjectsOfGroup,
  allVulnerabilitiesOfProject,
  newProjectLabel
} from './library/gitlab-api.js'


process.env["URL"] = "https://gitlab.com/api/v4/"
process.env["TOKEN"] = process.env.GITLAB_TOKEN_ADMIN


const gitLabClient = getGitLabClient({
  baseUri: process.env["URL"],
  token: process.env["TOKEN"]
})


const Groups = {
	CseDt: 58064323,
	TanukiWorkshops: 5085244
}

const Projects = {
  ExperimentWithApi: 41258941
}


console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::magic_report",
    color:"green",
    description:"report_type: magic_report"
  })
)




