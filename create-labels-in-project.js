//const gl = require("./library/gl-cli").GLClient

import {
  about, 
  getGitLabClient, 
  getAllDescendantGroups,
  allDescendantGroups,
  allProjectsOfGroup,
  allVulnerabilitiesOfProject,
  newProjectLabel
} from './library/gitlab-api.js'


process.env["URL"] = "https://gitlab.com/api/v4/"
process.env["TOKEN"] = process.env.GITLAB_TOKEN_ADMIN


const gitLabClient = getGitLabClient({
  baseUri: process.env["URL"],
  token: process.env["TOKEN"]
})


const Groups = {
	CseDt: 58064323,
	TanukiWorkshops: 5085244
}

const Projects = {
  ExperimentWithApi: 41258941
}

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"severity::info",
    color:"blue",
    description:"severity level: info"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"severity::unknown",
    color:"black",
    description:"severity level: unknown"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"severity::low",
    color:"yellow",
    description:"severity level: low"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"severity::medium",
    color:"orange",
    description:"severity level: medium"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"severity::high",
    color:"pink",
    description:"severity level: high"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"severity::critical",
    color:"red",
    description:"severity level: critical"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::sast",
    color:"green",
    description:"report_type: sast"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::dast",
    color:"green",
    description:"report_type: dast"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::dependency_scanning",
    color:"green",
    description:"report_type: dependency_scanning"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::container_scanning",
    color:"green",
    description:"report_type: container_scanning"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::coverage_fuzzing",
    color:"green",
    description:"report_type: coverage_fuzzing"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::api_fuzzing",
    color:"green",
    description:"report_type: api_fuzzing"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::cluster_image_scanning",
    color:"green",
    description:"report_type: cluster_image_scanning"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::license_scanning",
    color:"green",
    description:"report_type: license_scanning"
  })
)

console.log(
  await newProjectLabel({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi,
    name:"report_type::secret_detection",
    color:"green",
    description:"report_type: secret_detection"
  })
)



