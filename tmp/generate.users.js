/**
 * This script generates users in 2 separate GitLab instances
 * ⚠️ don't forget to count the admin user(s)
 * 
 * TODO: add catch to promises to check if all users are created
 * the most important thing is to know if there is the same number of users in each instance
 * 
 */

const GLClient = require("./libs/gl-cli").GLClient

/**
 * Instance of the GitLab client
 * you need to set the environment variables
 * 
 * ### How to run the script:
 * e.g.:
 * ```
 * TOKEN="DfaXK4wjins_ZnANyzJL" URL="http://gitlab-1.test" node generate.users.js gitlab-1
 * ```
 */
let gitLabClient = new GLClient({
  baseUri: `${process.env.URL}/api/v4`,
  token: process.env.TOKEN
})

const faker = require('faker');

async function createUser(gitLabConnection, {username, name, email, password}) {
  return gitLabConnection.createUser({username, name, email, password})
}

async function batch() {
  // generate distinct users in the instance
  try {
    for (i = 0; i <= 9; i++) { 
      let email = faker.internet.email()
      let username = name = email.substring(0, email.lastIndexOf("@"))
      let password = "ilovepandas"
      // await is only valid in async function
      await createUser(gitLabClient, {username, name, email, password})
    }
  } catch (error) {
    console.log("😡:", error)
  }
}

batch()