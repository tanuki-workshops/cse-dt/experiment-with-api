const GLClient = require("./libs/gl-cli").GLClient
module.exports = {
  instances: [
      new GLClient({
        baseUri: `${process.env.URL}/api/v4`,
        token: process.env.TOKEN
      })
  ]
}