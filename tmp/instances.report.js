const DEBUG = process.env.DEBUG || false

const md = require('markdown-it')()

const GLClient = require("./libs/gl-cli").GLClient
const fs = require("fs")
const jsonexport = require('jsonexport');
const url = require('url')


let instances = require("./instances.config").instances

/* access_level
    10 => Guest access
    20 => Reporter access
    30 => Developer access
    40 => Maintainer access
    50 => Owner access # Only valid for groups
*/
let getAccessLevelLabel = (level) => {
  switch(level) {
    case 10:
      return "guest"
      break;
    case 20:
      return "reporter"
      break;
    case 30:
      return "developer"
      break;
    case 40:
      return "maintener"
      break;
    case 50:
      return "owner"
      break;
    default:
      return ""
  }
} // end of getAccessLevelLabel


async function getAllUsers({glClient, perPage, page}) {
  return glClient.getAllUsers({perPage, page})
}

async function getAllProjects({glClient, perPage, page}) {
  return glClient.getAllProjects({perPage, page})
}

async function getAllGroups({glClient, perPage, page}) {
  return glClient.getAllGroups({perPage, page})
}

async function getActivities(glClient) {
  return glClient.getActivities()
}

// === Members ===
// https://docs.gitlab.com/ee/api/members.html
// Get Members of a project
async function getProjectMembers(glClient, {projectId}) {
  return glClient.getProjectMembers({projectId})
}

// Get Members of a project
async function getGroupMembers(glClient, {groupId}) {
  return glClient.getGroupMembers({groupId})
}

// await is only valid in async function
/**
 * Get all users of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceUsers(glClient) {
  try {
    var stop = false, page = 1, users = []
    while (!stop) {
      await getAllUsers({glClient, perPage:20, page:page}).then(someUsers => {
        users = users.concat(someUsers.map(user => {

          if(DEBUG) {
            console.log("user:", `${user.id} ${user.name} ${user.username} ${user.state} isAdmin: ${user.is_admin}`)
            //console.log("------------------------------------------------------------------------------")
          }

          //console.log(user)
          /* --- user information
            { id: 1,
              name: 'Administrator',
              username: 'root',
              state: 'active',
              avatar_url:
              'https://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon',
              web_url: 'http://gitlab-3.test/root',
              created_at: '2018-12-10T14:58:14.458Z',
              bio: null,
              location: null,
              public_email: '',
              skype: '',
              linkedin: '',
              twitter: '',
              website_url: '',
              organization: null,
              last_sign_in_at: '2018-12-10T15:02:29.652Z',
              confirmed_at: '2018-12-10T14:58:14.168Z',
              last_activity_on: '2018-12-10',
              email: 'admin@example.com',
              theme_id: 1,
              color_scheme_id: 1,
              projects_limit: 100000,
              current_sign_in_at: '2018-12-10T17:57:34.355Z',
              identities: [],
              can_create_group: true,
              can_create_project: true,
              two_factor_enabled: false,
              external: false,
              private_profile: null,
              is_admin: true,
              shared_runners_minutes_limit: null }
          */

          return { 
            id:user.id,
            username:user.username, 
            web_url:user.web_url, 
            instance_name: url.parse(user.web_url).hostname,
            state:user.state, 
            email:user.email,
            is_admin:user.is_admin,
            external:user.external,
            can_create_group: user.can_create_group,
            can_create_project: user.can_create_project,
            created_at: user.created_at,
            last_sign_in_at: user.last_sign_in_at,
            confirmed_at: user.confirmed_at,
            last_activity_on: user.last_activity_on,
            current_sign_in_at: user.current_sign_in_at
          }
        }))
        if(someUsers.length == 0) { stop = true }
      })
      page +=1
    }
    return users
  } catch (error) {
    console.log("😡:", error)
  }
}

/**
 * Get all projects of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceProjects(glClient) {
  try {
    var stop = false, page = 1, projects = []
    while (!stop) {
      await getAllProjects({glClient, perPage:20, page:page}).then(someProject => {
        
        projects = projects.concat(someProject.map(project => {
          return project
        }))
        if(someProject.length == 0) { stop = true }
      })
      page +=1
    }
    return projects
  } catch (error) {
    console.log("😡:", error)
  }
}

/**
 * Get all groups  of the GitLab instance
 * @param {*} glClient 
 */
async function getInstanceGroups(glClient) {
  try {
    var stop = false, page = 1, groups = []
    while (!stop) {
      await getAllGroups({glClient, perPage:20, page:page}).then(someGroup => {
        groups = groups.concat(someGroup.map(group => {
          return group
        }))
        if(someGroup.length == 0) { stop = true }
      })
      page +=1
    }
    return groups
  } catch (error) {
    console.log("😡:", error)
  }
}

async function batch(instances) {
  console.log(instances)
  var all_activities = []

  for(var i in instances) {
    let instanceGitLabClient = instances[i]

    let instanceName = url.parse(instanceGitLabClient.baseUri).hostname
    console.time("GitLab")
    var arrGLInstance
      , activitiesGLInstance
      , projectsGLInstance
      , groupsGLInstance
      , projectsMembersGLInstance
      , groupsMembersGLInstance

      await getInstanceUsers(instanceGitLabClient).then(res => arrGLInstance=res)
      await getActivities(instanceGitLabClient).then(res => activitiesGLInstance=res)
      await getInstanceProjects(instanceGitLabClient).then(res => projectsGLInstance=res)
      await getInstanceGroups(instanceGitLabClient).then(res => groupsGLInstance=res)
    
      fs.writeFileSync(`./reports/projects_${instanceName}.json`, JSON.stringify(projectsGLInstance))
      fs.writeFileSync(`./reports/groups_${instanceName}.json`, JSON.stringify(groupsGLInstance))
      jsonexport(projectsGLInstance, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./reports/projects_${instanceName}.csv`, csv)
      })
      jsonexport(groupsGLInstance, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./reports/groups_${instanceName}.csv`, csv)
      })        


      // ----- find the max role on project for each user -----
      projectsMembersGLInstance = []
      for(var i in projectsGLInstance) {
        await getProjectMembers(instanceGitLabClient, {projectId: projectsGLInstance[i].id})
          .then(res => {
            projectsMembersGLInstance = projectsMembersGLInstance.concat(res.map(item => {
              item.access_level_label = getAccessLevelLabel(item.access_level)
              return item
            }))
          })
      }

      fs.writeFileSync(`./reports/projects_members_${instanceName}.json`, JSON.stringify(projectsMembersGLInstance))
      jsonexport(projectsMembersGLInstance, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./reports/projects_members_${instanceName}.csv`, csv)
      })

      // ----- find the max role on Group for each user -----
      groupsMembersGLInstance = []
      for(var i in groupsGLInstance) {
        await getGroupMembers(instanceGitLabClient, {groupId: groupsGLInstance[i].id})
          .then(res => {
            groupsMembersGLInstance = groupsMembersGLInstance.concat(res.map(item => {
              item.access_level_label = getAccessLevelLabel(item.access_level)
              return item
            }))
          })
      }

      fs.writeFileSync(`./reports/groups_members_${instanceName}.json`, JSON.stringify(groupsMembersGLInstance))
      jsonexport(groupsMembersGLInstance, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./reports/groups_members_${instanceName}.csv`, csv)
      })

      /** activities affectations */
      arrGLInstance.forEach(userInstance => {

        userInstance.project_access_level = null
        userInstance.project_access_level_label = null
        userInstance.group_access_level = null
        userInstance.group_access_level_label = null

        var userInProject = projectsMembersGLInstance.find(user => user.id == userInstance.id)
        if(userInProject) {
          var userInAllProjects = projectsMembersGLInstance.filter(user => user.id == userInstance.id)
          let maxLevel = Math.max.apply(Math, userInAllProjects.map(o => { return o.access_level }))
          //userInstance.project_access_level = userInProject.access_level
          userInstance.project_access_level = maxLevel
          userInstance.project_access_level_label = getAccessLevelLabel(maxLevel)
        }
        
        var userInGroup = groupsMembersGLInstance.find(user => user.id == userInstance.id)
        if(userInGroup) {
          var userInAllGroups = groupsMembersGLInstance.filter(user => user.id == userInstance.id)
          let maxLevel = Math.max.apply(Math, userInAllGroups.map(o => { return o.access_level }))
          //userInstance.group_access_level = userInGroup.access_level
          userInstance.group_access_level = maxLevel
          userInstance.group_access_level_label = getAccessLevelLabel(maxLevel)
        }
      })      
      

      fs.writeFileSync(`./reports/data_${instanceName}.json`, JSON.stringify(arrGLInstance))
      jsonexport(arrGLInstance, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./reports/data_${instanceName}.csv`, csv)
      })
      
      all_activities = all_activities.concat(arrGLInstance)
      /*
      arrGLInstance.forEach(item => {
        all_activities.push(item)
      })
      */

      console.timeEnd("GitLab")

  } // end for

  fs.writeFileSync(`./reports/data_all_instances.json`, JSON.stringify(all_activities))
  jsonexport(all_activities, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./reports/data_all_instances.csv`, csv)
  })

  /* === record structure ===
  { 
    id: 1,
    username: 'root',
    web_url: 'http://gitlab2.test/root',
    instance_name: 'gitlab2.test',
    state: 'active',
    email: 'admin@example.com',
    is_admin: true,
    external: false,
    can_create_group: true,
    can_create_project: true,
    created_at: '2019-01-28T19:24:02.465Z',
    last_sign_in_at: '2019-01-28T19:29:06.026Z',
    confirmed_at: '2019-01-28T19:24:02.072Z',
    last_activity_on: '2019-01-28',
    current_sign_in_at: '2019-01-28T19:29:06.026Z',
    project_access_level: null,
    project_access_level_label: null,
    group_access_level: null,
    group_access_level_label: null 
  }
  */

  let report = all_activities.map(userRecord => {
    //console.log(userRecord)
    let maxAccessLevel = 
      userRecord.project_access_level < userRecord.group_access_level
      ? userRecord.group_access_level
      : userRecord.project_access_level
    
    userRecord.maxAccessLevel = maxAccessLevel
    userRecord.maxAccessLevelLabel = getAccessLevelLabel(maxAccessLevel)
    userRecord.checked = false
    return userRecord
  })

  fs.writeFileSync(`./reports/report.json`, JSON.stringify(report))
  jsonexport(report, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./reports/report.csv`, csv)
  })

  var tmp_consolidated_report = []

  var all_duplicates = []

  report.forEach(userRecord => {
    if(userRecord.checked) {

    } else {

      let duplicates = report.filter(user => user.username == userRecord.username && user.email == userRecord.email && user.checked == false)
      if(duplicates.length > 1) {

        all_duplicates = all_duplicates.concat(duplicates)

        let maxAccessLevelFromDuplicates = Math.max.apply(Math, duplicates.map(user => user.maxAccessLevel))
        let maxUser = duplicates.find(user => user.maxAccessLevel == maxAccessLevelFromDuplicates)
        
        if(maxUser) {
          tmp_consolidated_report.push(maxUser)
        } else {
          // if the user has no max access level we take the first of the duplicates
          tmp_consolidated_report.push(duplicates[0]) 
        }

        duplicates.forEach(item => item.checked = true)

      } else {
        userRecord.checked = true
        tmp_consolidated_report.push(userRecord)
      }
    }
  })


  let consolidated_report = tmp_consolidated_report.map(user => {
    return {
      id: user.id,
      username: user.username,
      web_url: user.web_url,
      instance_name: user.instance_name,
      state: user.state,
      email: user.email,
      is_admin: user.is_admin,
      external: user.external,
      maxAccessLevel: user.maxAccessLevel,
      maxAccessLevelLabel: user.maxAccessLevelLabel
    }
  })

  let levels = [
      {maxAccessLevel:10, maxAccessLevelLabel:"guest"}
    , {maxAccessLevel:20, maxAccessLevelLabel:"reporter"}
    , {maxAccessLevel:30, maxAccessLevelLabel:"developer"}
    , {maxAccessLevel:40, maxAccessLevelLabel:"maintener"}
    , {maxAccessLevel:50, maxAccessLevelLabel:"owner"}
    , {maxAccessLevel:null, maxAccessLevelLabel:"no access level"}
  ]
  //console.log(levels)


  fs.writeFileSync(`./reports/consolidated_report.json`, JSON.stringify(consolidated_report))
  jsonexport(consolidated_report, {rowDelimiter: ';'}, function(err, csv){
    if(err) return console.log(err)
    fs.writeFileSync(`./reports/consolidated_report.csv`, csv)
  })


  return all_activities
}

batch(instances).then(result => {
  console.log(result.length)
})

