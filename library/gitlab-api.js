import axios from 'axios'

function about() {
  return "GitLab API by @k33g"
}

class GitLabClient {
  constructor({ baseUri, token }) {
    this.baseUri = baseUri;
    this.token = token;
    this.headers = {
      "Content-Type": "application/json",
      "Private-Token": token
    };
  }

  callAPI({ method, path, data }) {
    return axios({
      method: method,
      url: this.baseUri + path,
      headers: this.headers,
      data: data !== null ? JSON.stringify(data) : null
    });
  }

  get({ path }) {
    return this.callAPI({
      method: "GET",
      path,
      data: null
    });
  }

  delete({ path }) {
    return this.callAPI({
      method: "DELETE",
      path,
      data: null
    });
  }

  post({ path, data }) {
    return this.callAPI({
      method: "POST",
      path,
      data
    });
  }
} 

function getGitLabClient({ baseUri, token }) {
  return new GitLabClient({ baseUri, token })
}

// https://docs.gitlab.com/ee/api/groups.html#list-a-groups-descendant-groups
// GET /groups/:id/descendant_groups
// Get a list of visible descendant groups of this group

function getAllDescendantGroups({gitlabClient, groupId, perPage, page}) {
  return gitlabClient   // return a promise
    .get({path: `/groups/${groupId}/descendant_groups?per_page=${perPage}&page=${page}`})
    .then(response => response.data)
    .catch(error => error)
}

async function asyncGetAllDescendantGroups({gitlabClient, groupId, perPage, page}) {
  return getAllDescendantGroups({gitlabClient, groupId, perPage, page})
}

async function allDescendantGroups({gitlabClient, groupId}) {
  try {
    var stop = false, page = 1, groups = []
    while (!stop) {
      await asyncGetAllDescendantGroups({
        gitlabClient, groupId, perPage:20, page: page
      }).then(someGroups => {

        if(someGroups.length == 0) { stop = true }
        groups = groups.concat(someGroups.map(group => group))
      })
      page +=1
    } 
    return groups

  } catch (error) {
    console.log("😡 allDescendantGroups:", error)
  }
}

// https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects
// GET /groups/:id/projects
// Get a list of projects in this group.

function getAllProjectsOfGroup({gitlabClient, groupId, perPage, page}) {
  return gitlabClient   // return a promise
    .get({path: `/groups/${groupId}/projects?per_page=${perPage}&page=${page}`})
    .then(response => response.data)
    .catch(error => error)
}

async function asyncGetAllProjectsOfGroup({gitlabClient, groupId, perPage, page}) {
  return getAllProjectsOfGroup({gitlabClient, groupId, perPage, page})
}

async function allProjectsOfGroup({gitlabClient, groupId}) {
  try {
    var stop = false, page = 1, projects = []
    while (!stop) {
      await asyncGetAllProjectsOfGroup({
        gitlabClient, groupId, perPage:20, page: page
      }).then(someProjects => {

        if(someProjects.length == 0) { stop = true }
        projects = projects.concat(someProjects.map(project => project))
      })
      page +=1
    } 
    return projects

  } catch (error) {
    console.log("😡 allProjectsOfGroup:", error)
  }
}

// https://docs.gitlab.com/ee/api/vulnerability_findings.html
// List project vulnerability findings
// GET /projects/:id/vulnerability_findings

function getAllVulnerabilitiesOfProject({gitlabClient, projectId, perPage, page}) {
  return gitlabClient   // return a promise
    .get({path: `/projects/${projectId}/vulnerability_findings?per_page=${perPage}&page=${page}`})
    .then(response => response.data)
    .catch(error => error)
}

async function asyncGetAllVulnerabilitiesOfProject({gitlabClient, projectId, perPage, page}) {
  return getAllVulnerabilitiesOfProject({gitlabClient, projectId, perPage, page})
}


async function allVulnerabilitiesOfProject({gitlabClient, projectId}) {
  try {
    var stop = false, page = 1, vulnerabilities = []
    while (!stop) {
      await asyncGetAllVulnerabilitiesOfProject({
        gitlabClient, projectId, perPage:20, page: page
      }).then(someVulnerabilities => {

        if(someVulnerabilities.length == 0) { stop = true }
        vulnerabilities = vulnerabilities.concat(someVulnerabilities.map(vulnerability => vulnerability))
      })
      page +=1
    } 
    return vulnerabilities

  } catch (error) {
    console.log("😡 allVulnerabilitiesOfProject:", error)
  }
}

// https://docs.gitlab.com/ee/api/labels.html#create-a-new-label
// POST /projects/:id/labels
// Creates a new label for the given repository with the given name and color.

function createProjectLabel({ gitlabClient, projectId, name, color, description }) {
  return gitlabClient
    .post({
      path: `/projects/${projectId}/labels`,
      data: { name, color, description }
    })
    .then(response => {
      return response.data
    })
    .catch(error => {
      return error
    })
}

async function asyncCreateProjectLabel({gitlabClient, projectId, name, color, description}) {
  return createProjectLabel({gitlabClient, projectId, name, color, description})
}

async function newProjectLabel({gitlabClient, projectId, name, color, description}) {
  try {
    return await asyncCreateProjectLabel({gitlabClient, projectId, name, color, description})
    .then(result => result)
  } catch(error) {
    console.log("😡 newProjectLabel:", error)
  }
  
}

// https://docs.gitlab.com/ee/api/issues.html#new-issue
// POST /projects/:id/issues


function createIssue({gitlabClient, projectId, title, labels, description, created_at}) {
  return gitlabClient
    .post({
      path: `/projects/${projectId}/issues`,
      data: { title, labels, description, created_at}
    })
    .then(response => {
      return response.data
    })
    .catch(error => {
      return error
    })
}

async function asyncCreateIssue({gitlabClient, projectId, title, labels, description, created_at}) {
  return createIssue({gitlabClient, projectId, title, labels, description, created_at})
}

async function newIssue({gitlabClient, projectId, title, labels, description, created_at}) {
  try {
    return await asyncCreateIssue({gitlabClient, projectId, title, labels, description, created_at})
    .then(result => result)
  } catch(error) {
    console.log("😡 newIssue:", error)
  }
  
}




export {
  about,
  getGitLabClient,
  getAllDescendantGroups,
  allDescendantGroups,
  getAllProjectsOfGroup,
  allProjectsOfGroup,
  getAllVulnerabilitiesOfProject,
  allVulnerabilitiesOfProject,
  createProjectLabel,
  newProjectLabel,
  createIssue,
  newIssue
}
