//const gl = require("./library/gl-cli").GLClient

import {
  about, 
  getGitLabClient, 
  getAllDescendantGroups,
  allDescendantGroups,
  allProjectsOfGroup,
  allVulnerabilitiesOfProject,
  newIssue
} from './library/gitlab-api.js'

import * as fs from 'fs'

process.env["URL"] = "https://gitlab.com/api/v4/"
process.env["TOKEN"] = process.env.GITLAB_TOKEN_ADMIN


console.log(about())

const gitLabClient = getGitLabClient({
  baseUri: process.env["URL"],
  token: process.env["TOKEN"]
})


const Groups = {
	CseDt: 58064323,
	TanukiWorkshops: 5085244
}

const Projects = {
  ExperimentWithApi: 41258941
}

// 58064323

/*
getAllDescendantGroups({
  gitlabClient: gitLabClient,
  groupId: Groups.TanukiWorkshops,
  perPage: 20,
  page:1
}).then(data => {
  console.log(data)
}).catch(error => {
  console.log(error)
})
*/

/*
var groups; 
await allDescendantGroups({
  gitlabClient: gitLabClient,
  groupId: Groups.TanukiWorkshops
}).then(result => {
  console.log(result)
})
*/

let allVulnerabilities = []

// Get all the sub groups of a group
let groups = await allDescendantGroups({
  gitlabClient: gitLabClient,
  groupId: Groups.CseDt
})


// Get all projects of all sub groups of the group
for(var i in groups) {
  let currentGroup = groups[i]
  console.log(currentGroup.id, currentGroup.name, currentGroup.path)
  
  let projects = await allProjectsOfGroup({
    gitlabClient: gitLabClient,
    groupId: currentGroup.id
  })


  // build an array of all vulnerabilities
  for(var i in projects) {
    let currentProject = projects[i]
    console.log("  ->", currentProject.id, currentProject.name, currentProject.path_with_namespace)
    
    let projectVulnerabilities = await allVulnerabilitiesOfProject({
      gitlabClient: gitLabClient,
      projectId: currentProject.id
    })

    

    
    for(var i in projectVulnerabilities) {
      let vulnerability = projectVulnerabilities[i]
      console.log("    =>", vulnerability.uuid, vulnerability.name)
      allVulnerabilities.push(vulnerability)
    }
    

  }
}

// Create all the issues
for(var i in allVulnerabilities) {
  let vulnerability = allVulnerabilities[i]

  let title = vulnerability.name
  let labels = `severity::${vulnerability.severity},report_type::${vulnerability.report_type}`

  let content = []
  content.push(`- **confidence**: ${vulnerability.confidence}`)
  content.push(`- **scanner**: ${vulnerability.scanner.name} | ${vulnerability.scanner.vendor}`)
  content.push(`- **project**: ${vulnerability.project.id} | ${vulnerability.project.full_path}`)
  content.push(`- **state**: ${vulnerability.state}`)
  content.push(`- **path**: [${vulnerability.blob_path}](${vulnerability.blob_path})`)

  content.push(``)
  content.push(`### Description`)
  content.push(`${vulnerability.description}`)

  content.push(`### Links`)
  content.push("```json")
  content.push(`${JSON.stringify(vulnerability.links, null, 2)}`)
  content.push("```")

  content.push(`### Location`)
  content.push("```json")
  content.push(`${JSON.stringify(vulnerability.location, null, 2)}`)
  content.push("```")

  content.push(`### Solution`)
  content.push(`${vulnerability.solution}`)

  content.push(`### Scan`)
  content.push("```json")
  content.push(`${JSON.stringify(vulnerability.scan, null, 2)}`)
  content.push("```")


  let issue = newIssue({
    gitlabClient: gitLabClient,
    projectId: Projects.ExperimentWithApi, // this is the demo project
    title: title,
    labels: labels,
    description: content.join("\r\n"),
    created_at: vulnerability.scan.end_time
  })
  
  console.log(issue)

}

//fs.writeFileSync(`./vulnerabilities.json`, JSON.stringify(allVulnerabilities))


/* vulnerability:



*/